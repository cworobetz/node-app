CREATE DATABASE app;
\connect app
CREATE TABLE users (
        id serial PRIMARY KEY,
        username VARCHAR(30) UNIQUE NOT NULL,
        password_salt VARCHAR(16) NOT NULL,
        password VARCHAR(255) NOT NULL,
        registration_date TIMESTAMP NOT NULL DEFAULT now()
);
