const pgp = require('pg-promise')();

// Database connection import and setup
var connection = {
    host: 'postgres',
    port: '5432',
    database: 'app',
    user: 'sysadmin',
    password: 'notarealpassword'
}
var db = pgp(connection);

module.exports = db;