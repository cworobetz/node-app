// Imports all at the top
const bodyParser = require("body-parser");
const express = require('express');

// Assigning an instance of express to app
const app = express();
const PORT = 3000;

// We'll also need bodyParser to handle POST requests
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

// Set up routes
var user = require('./routes/user.js');
app.use('/user', user);

app.listen(PORT, () => console.log(`Express is running on port ${PORT}`))