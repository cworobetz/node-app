// Imports all at the top
const express = require('express');
const crypto = require('crypto')
const router = express.Router();
const db = require('../db.js')

// Methods that I wish to implement
router.get('/', (req, res) => {
    db.any('SELECT id, username, password, password_salt, registration_date FROM users')
        .then(users => {
            res.status(200).send({
                users
            });
        })
        .catch(error => {
            res.status(400).send({
                "error": error
            })
        });
});

router.delete('/', (req, res) => {
    var id = req.body.id;

    db.none('DELETE FROM users WHERE id = $1', [id])
        .then(() => {
            res.status(204).send({});
        })
        .catch(error => {
            res.status(400).send({
                "error": error
            });
        });
});

router.post('/', (req, res) => {

    var username = req.body.username;
    var plaintextPassword = req.body.password;

    var genRandomString = function (length) {
        return crypto.randomBytes(Math.ceil(length / 2))
            .toString('hex') /** convert to hexadecimal format */
            .slice(0, length); /** return required number of characters */
    };

    var sha512 = function (password, salt) {
        var hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
        hash.update(password);
        var value = hash.digest('hex');
        return {
            salt: salt,
            passwordHash: value
        };
    };

    function saltHashPassword(userpassword) {
        var salt = genRandomString(16); /** Gives us salt of length 16 */
        var passwordData = sha512(userpassword, salt);

        return {
            salt: salt,
            password: passwordData.passwordHash
        }
    }

    var passwordData = saltHashPassword(plaintextPassword);
    var password = passwordData.password;
    var passwordSalt = passwordData.salt;

    db.none('INSERT INTO users (username, password, password_salt) VALUES ($1, $2, $3)', [username, password, passwordSalt])
        .then(() => {
            res.status(201).send({
                "status": 'success'
            });
        })
        .catch(error => {
            res.status(400).send({
                "error": error
            })
        });

});

module.exports = router